This Tomb Raider 2 version is released on the Steam platform.

The timestamps of the files are not preserved, this is a snapshot from the version of 2018-06-10.

Info:
- Has FMV disable option
- Very weird gameplay mechanics, standing jumps/sideflips in low ceiling areas result into a stumble. Otherwise close to the Botched version.
- Doesn't need a CD
