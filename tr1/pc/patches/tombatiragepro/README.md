This patch was released by Eidos and is still available under the following link:
https://web.archive.org/web/20081208051118/http://ftp.eidos-france.fr/pub/fr/tomb_raider/patches/tombatiragepro.zip

The patch doesn't seem to be restricted to any particular version of the game, so it can be technically used together with any previously released game disc of TR1.

Due to a bug crashing the game when an atlantean centaur or the giant atlantean in the great pyramid dies, it is currently impossible to finish the game with this version.
More Info:
https://github.com/ata4/glrage/blob/master/glrage_patch/TombRaiderPatch.cpp#L59
